# Installation
To install the package, go to `dist` directory and then use `pip install edudata-1.tar.gz` or `pip install edudata-1-py3-none-any.whl`. To use it, simply write `import edustats`.

# Usage
You first need to create an instance of `EduData`. Then you can pass it to functions to perform calculations.
### EduData
`class edustats.EduData(path_lud, path_schools)`
This class stores data read from excel files.
###### Parameters
`path_lud` path to file containing data about inhabitants (can be found in the data directory) <br/>
`path_schools` path to file containing data about schools (can be found in the data directory)
###### Methods
`EduData.sch_types` returns available school types <br/>
`EduData.gm_types` returns available gmina types
### gm_school
`edustats.gm_school(edu, gmina, typ, year)` <br/>
Calculate average number of students per school broken down by their year of birth and type of school in a district.
###### Parameters
`edu` - instance of `edustats.EduData` <br/> <br/>
`gmina` - name of Gmina <br/>
`typ` - school type<br/>
`year` - year of birth (should be between 1998 and 2015) <br/>
### gm_teacher
`edustats.gm_teacher(edu, gmina, typ)` <br/> <br/>
Calculate basic statistics of students per teacher broken down by the type of school in a district. 
###### Parameters
`edu` - instance of `edustats.EduData` <br/> <br/>
`gmina` - name of Gmina <br/>
`typ` - school type<br/>
### pl_school
`edustats.pl_school(edu, gmina_typ, typ, year)` <br/> <br/>
Calculate average number of students per school broken down by their year of birth and type of shool in total in rural/ mixed districts or in cities.
###### Parameters
`edu` - instance of `edustats.EduData` <br/> <br/>
`gmina_typ` - type of Gmina. `'M'` - city, `'M-Gm'` - mixed, `'Gm'` <br/>
`typ` - school type<br/>
`year` - year of birth (should be between 1998 and 2015) <br/>
### pl_teacher
`edustats.pl_teacher(edu, gmina_typ, typ)` <br/> <br/>
Calculate basic statistics of students per teacher broken down by the type of school in total in rural/ mixed districts.
###### Parameters
`edu` - instance of `edustats.EduData` <br/>
`gmina_typ` - type of Gmina. `'M'` - city, `'M-Gm'` - mixed, `'Gm'` -rural <br/>
`typ` - school type<br/>
