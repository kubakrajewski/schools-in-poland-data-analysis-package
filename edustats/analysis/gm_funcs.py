import numpy as np


def gm_teacher(es, gmina, typ):
    schools = es._schools
    lud = es._lud
    # check if gmina exists
    if es._schools[es._schools['Gmina'] == gmina].shape[0] == 0:
        raise Exception('no Gmina named ' + gmina)
    # check if type exists
    if es._schools[
        (es._schools['Typ'] == typ) &
        (es._schools['Gmina'] == gmina)
    ].shape[0] == 0:
        raise Exception('in the district ' + gmina + ' no school type: ' + typ + '\n try using sch_types function')

    # 0 students
    s = schools[
        (schools['Gmina'] == gmina) &
        (schools['Typ'] == typ) &
        (schools['Uczniowie'] == 0)
    ].shape[0]
    # 0 teachers
    t = schools[
        (schools['Gmina'] == gmina) &
        (schools['Typ'] == typ) &
        (schools['Nauczyciele'] == 0)
    ].shape[0]
    # print 0 teachers
    print('warning: ' + str(t) + ' school(s) with 0 teachers found in ' + gmina)
    print('warning: ' + str(s) + ' school(s) with 0 students found in ' + gmina)
    # calculate and return
    return schools[
        (schools['Gmina'] == gmina) &
        (schools['Typ'] == typ) &
        (schools['Stosunek'] != 0) &
        (schools['Stosunek'] != np.inf)
    ]['Stosunek'].describe()


def gm_school(es, gmina, typ, year):
    year = 2018 - year
    schools = es._schools
    lud = es._lud
    # check if gmina exists
    if es._schools[es._schools['Gmina'] == gmina].shape[0] == 0:
        raise Exception('no Gmina named ' + gmina)
    # check if type exists
    if es._schools[
        (es._schools['Typ'] == typ) &
        (es._schools['Gmina'] == gmina)
    ].shape[0] == 0:
        raise Exception('in the district ' + gmina + ' no school type: ' + typ + '\n try using sch_types function')
    # check if the year is proper
    if year > 19 or year < 3:
        raise Exception('not a proper age: ' + str(year))

    students = int(lud[
        (lud['Gmina'] == gmina) &
        (lud['Wiek'] == year)
    ]['Osoby'].aggregate('sum'))
    n_schools = schools[
        (schools['Gmina'] == gmina) &
        (schools['Typ'] == typ)
    ].shape[0]
    return students/n_schools
