import numpy as np


def pl_school(es, gmina_typ, typ, year):
    year = 2018 - year
    schools = es._schools
    lud = es._lud
    # check if gmina typ exists
    if es._schools[es._schools['Typ gminy'] == gmina_typ].shape[0] == 0:
        raise Exception('no Gmina type named ' + gmina_typ + '\n try using gm_types function')
    # check if type exists
    if es._schools[es._schools['Typ'] == typ].shape[0] == 0:
        raise Exception('no school type: ' + typ + '\n try using sch_types function')
    # check if the year is proper
    if year > 19 or year < 3:
        raise Exception('not a proper age: ' + str(year))

    def typ_gminy(gmina):
        typ_df = schools[schools['Gmina'] == gmina]
        if typ_df.shape[0] == 0:
            return ''
        return typ_df.iloc[0, 1]

    lud_age = lud[lud['Wiek'] == year]
    students = lud_age[(lud_age['Gmina'].apply(typ_gminy)) == gmina_typ]['Osoby'].aggregate('sum')

    n_schools = schools[
        (schools['Typ gminy'] == gmina_typ) &
        (schools['Typ'] == typ)
        ].shape[0]
    return students / n_schools

def pl_teacher(es, gmina_typ, typ):
    # check if gmina typ exists
    if es._schools[es._schools['Typ gminy'] == gmina_typ].shape[0] == 0:
        raise Exception('no Gmina type named ' + gmina_typ + '\n try using gm_types function')
    # check if type exists
    if es._schools[es._schools['Typ'] == typ].shape[0] == 0:
        raise Exception('no school type: ' + typ + '\n try using sch_types function')

    # 0 students
    s = es._schools[
        (es._schools['Typ gminy'] == gmina_typ) &
        (es._schools['Typ'] == typ) &
        (es._schools['Uczniowie'] == 0)
        ].shape[0]
    # 0 teachers
    t = es._schools[
        (es._schools['Typ gminy'] == gmina_typ) &
        (es._schools['Typ'] == typ) &
        (es._schools['Nauczyciele'] == 0)
        ].shape[0]
    # print 0 teachers
    print('warning: ' + str(t) + ' school(s) with 0 teachers found')
    print('warning: ' + str(s) + ' school(s) with 0 students found')
    # calculate and return
    return es._schools[
        (es._schools['Typ gminy'] == gmina_typ) &
        (es._schools['Typ'] == typ) &
        (es._schools['Stosunek'] != 0) &
        (es._schools['Stosunek'] != np.inf)
        ]['Stosunek'].describe()
