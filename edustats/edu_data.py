class EduData:
    """A class for analysis of some educational data in Poland

    Args:
        path_lud: path to file including data about inhabitants
        path_schools: path to file including data about schools

    Attributes:
        _lud: data frame containing data about inhabitants
        _schools: data frame containing data about schools
    """

    def __init__(self, path_lud, path_schools):
        from . import read_data
        self._lud = read_data.lud.lud_prepare(path_lud)
        self._schools = read_data.schools.sch_prepare(path_schools)

    def sch_types(self):
        return set(self._schools['Typ'])

    def gm_types(self):
        return set(self._schools['Typ gminy'])
