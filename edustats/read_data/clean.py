# replace city districts by proper Gmina names
wwa = ['Bemowo', 'Białołęka', 'Mokotów', 'Ochota', 'Rembertów', 'Targówek', 'Śródmieście', 'Praga-Południe',
       'Ursus', 'Ursynów', 'Wawer', 'Wesoła', 'Wilanów', 'Wola', 'Włochy', 'Żoliborz', 'Praga-Północ']
pzn = ['Poznań-Grunwald', 'Poznań-Jeżyce', 'Poznań-Nowe Miasto', 'Poznań-Stare Miasto', 'Poznań-Wilda']
wrc = ['Wrocław-Fabryczna', 'Wrocław-Krzyki', 'Wrocław-Psie Pole', 'Wrocław-Stare Miasto', 'Wrocław-Śródmieście']
ldz = ['Łódź-Bałuty', 'Łódź-Górna', 'Łódź-Polesie', 'Łódź-Widzew', 'Łódź-Śródmieście']

# this function repairs inconsistencies of data found in both files
def city_repair(x):
    if isinstance(x, str):
        x = x.replace("M. ", "")
        x = x.replace("st. ", "")
        if x in wwa:
            x = 'Warszawa'
        elif x in pzn:
            x = 'Poznań'
        elif x in wrc:
            x = 'Wrocław'
        elif x in ldz:
            x = 'Łódź'
    return x