import pandas as pd
from . import clean

g = ""

# names of unnecessary rows
badcells = [
 '       1 ', '       2 ', '       20', '       21', '       22', '       23', '       24', '       25',
 '       26', '       27', '       28', '       29', '     15-64   lat              ', '     18-59 lat  kobiety       ',
 '     18-64 lata mężczyźni     ', '     19-24   lat              ', '     45-59 lat  kobiety       ',
 '     45-64 lata mężczyźni     ', '     60 lat i więcej kobiety  ', '     65 lat i więcej          ',
 '     65 lat i więcej mężczyźni', '    0    -    4', '    20   -    24', '    25   -    29',
 '    30   -    34', '    35   -    39', '    40   -    44', '    45   -    49', '    5    -    9', '    50   -    54',
 '    55   -    59', '    60   -    64', '    65   -    69', '    70   -    74', '    70 i więcej', '    75   -    79',
 '    75 i więcej', '    80   -    84', '    80 i więcej', '    85 i więcej', '   Biologiczne grupy wieku    ',
 '   Edukacyjne grupy wieku     ', '   Kobiety w wieku 15-49 lat  ', '   Wiek  mobilny              ',
 '   Wiek  niemobilny           ', '   Wiek  poprodukcyjny        ', '   Wiek  produkcyjny          ',
 '   Wiek  przedprodukcyjny     ', '      0-14   lat              ', '      3- 6   lat              ',
 '      7-12   lat              ', '     13-15   lat              ', '     16-18   lat              ',
 '    10   -    14', '    15   -    19']


def lud_prepare(path):
    print('Loading data (inhabitatns)...')
    lud = {}
    sheet = pd.read_excel(path, sheet_name=None)
    for woj in sheet:
        sheet[woj].columns = ["c" + str(i) for i in range(len(sheet[woj].columns))]
    for woj in sheet:
        lud[woj] = sheet[woj].iloc[7:, :]
    lud = pd.concat(lud)
    lud = lud[['c0', 'c1', 'c2']]
    lud['c0'] = lud['c0'].apply(clean.city_repair)

    print('Cleaning data (inhabitants)...')
    # remove unnecessary rows
    lud = pd.DataFrame(lud.loc[~lud['c0'].isin(badcells)])
    # add column containing Gmina name
    for i in range(lud.shape[0]):
        s = lud.iloc[i, 0]
        if isinstance(s, str) and not any(char.isdigit() for char in s):
            g = s
            continue
        lud.iloc[i, 1] = g
        lud.iloc[i, 0] = int(lud.iloc[i, 0])

    # give proper column names to lud
    lud.columns = ['Wiek', 'Gmina', 'Osoby']
    return lud
