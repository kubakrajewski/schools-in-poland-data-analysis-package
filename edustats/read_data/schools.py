import pandas as pd
from . import clean


def sch_prepare(path):
    print('Loading data (schools)...')
    schools = pd.read_excel(path)
    print('Cleaning data (schools)...')
    schools['Gmina'] = schools['Gmina'].apply(clean.city_repair)
    schools['Nauczyciele'] = (schools['Nauczyciele pełnozatrudnieni'] +
                              schools['Nauczyciele niepełnozatrudnieni (stos.pracy)'])
    schools = schools[['Gmina', 'Typ gminy', 'Nazwa typu', 'Nauczyciele',
                       'Uczniowie, wychow., słuchacze']]
    schools.columns = ['Gmina', 'Typ gminy', 'Typ', 'Nauczyciele', 'Uczniowie']
    schools['Stosunek'] = (schools['Uczniowie'] / schools['Nauczyciele'])
    schools = schools.iloc[1:, :]
    return schools
