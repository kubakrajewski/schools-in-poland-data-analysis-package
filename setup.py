import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
 long_description = fh.read()
setuptools.setup(
 name="edudata", 
 version = '1',
 author="Jakub Krajewski",
 author_email="jk406798@students.mimuw.edu.pl",
 description="A package for analysis of some edu data in Poland",
 long_description=long_description,
 long_description_content_type="text/markdown",
 packages=setuptools.find_packages(),
 classifiers=[
 "Programming Language :: Python :: 3",
 "License :: OSI Approved :: MIT License",
 "Operating System :: OS Independent",
 ],
 install_requires=[
 "pandas",
 "numpy"
 ],
 python_requires='>=3.6',
)
