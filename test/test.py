import edustats

es = edustats.EduData('tabela12.xls', 'szkoly.xlsx')

def test_gmina(gmina):
    print('\nTests for ' + gmina + ':')
    print('Szkoła podstawowa...')
    ex1 = edustats.gm_school(es, gmina, 'Szkoła podstawowa', 2002)
    print('Przedszkole...')
    ex2 = edustats.gm_school(es, gmina, 'Przedszkole', 2014)
    print('Liceum ogólnokształcące...')
    ex3 = edustats.gm_school(es, gmina, 'Liceum ogólnokształcące', 2005)

    print('Szkoła podstawowa...')
    ex6 = edustats.gm_teacher(es, gmina, 'Szkoła podstawowa')
    print('Przedszkole...')
    ex7 = edustats.gm_teacher(es, gmina, 'Przedszkole')
    print('Liceum ogólnokształcące...')
    ex8 = edustats.gm_teacher(es, gmina, 'Liceum ogólnokształcące')

    print('\nAverage number of children born in 2002 per Szkoła Podstawowa: ' + str(ex1))
    print('\nAverage number of children born in 2014 per Przedszkole: ' + str(ex2))
    print('\nAverage number of children born in 2005 per Liceum ogólnokształcące: ' + str(ex3))

    print('Statistics for number of children per teacher in Szkoła Podstawowa:')
    print(ex6)
    print('Statistics for number of children per teacher in Przedszkole:')
    print(ex7)
    print('Statistics for number of children per teacher in Liceum Ogólnokształcące:')
    print(ex8)


test_gmina('Warszawa')
test_gmina('Hajnówka')
test_gmina('Bydgoszcz')

print('\nAverage number of children born in 2008 per Szkoła podstawowa in cities:')
print(edustats.pl_school(es, 'M', 'Szkoła podstawowa', 2008))

print('\nStatistics for number of children per teacher in Liceum ogólnokształcące in rural districts:')
print(edustats.pl_teacher(es, 'M', 'Liceum ogólnokształcące'))

print('\nExceptions...')
# Test if exceptions work properly
try:
    edustats.gm_teacher(es, 'Bydgoszcz', 'Szkoła Kosmiczna')
except Exception as e:
    print(e)

try:
    edustats.gm_school(es, 'London', 'Szkoła podstawowa', 2002)
except Exception as e:
    print(e)

try:
    edustats.gm_school(es, 'Bydgoszcz', 'Szkoła podstawowa', 1000)
except Exception as e:
    print(e)

try:
    edustats.pl_teacher(es, 'Pole Namiotowe', 'Przedszkole')
except Exception as e:
    print(e)

try:
    edustats.pl_school(es, 'Osada', 'Przedszkole', 2002)
except Exception as e:
    print(e)
